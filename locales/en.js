export default {
  'main': {
    'authorization': 'Authorization',
    'login': 'Login',
    'add': 'Add',
    'remove': 'Remove',
    'send': 'Send',
    'friends': 'Friends',
    'logout': 'Logout',
    'home': 'Home',
    'done': 'Done',
  },
  'pages': {
    'login': {
      'text': 'Enter your login to get different options for the request. Options: '+
        '1) admin - successful login. '+
        '2) user - error of the login field. '+
        '3) user2 - server error. '+
        '4) any other option - pop-up form error (user banned)',
    },
    'index': {
      'help1': 'Here you can create a list of possible friends, and {link} (for authorized ones)',
      'help1_link': 'something interesting here',
      'help2': 'When you submit your friend list, you will get errors for each name. \n' +
        'If you send the list after the expiration of the authorization period (10 seconds), then you will be transferred to the authorization page',
    },
    'books': {
      'books': 'Interesting page',
      'text': 'Welcome! Go to the desired page. ',
      'links_list': 'Link list:',
      'links': {
        'link1': 'Non-existent page',
        'link2': 'Book page',
        'link3': 'Text quality rating',
      },
      'text_evaluation': 'Here you can write and send a text and get a rating! The inscribed text will be '+
        'will be saved in the store, but the score will only be saved if the text is successful. An unsuccessful option can be obtained, '+
        'if you submit a form without text. Successful options - enter var1 or var2 ',
      'text_grade': 'Your last text grade in points: {grade}',
    },
  },
  'request': {
    'errors': {
      'forbidden': 'No access',
      'internal_server_error': 'Server problem',
      'disconnect': 'No internet connection',
      'unauthorized': 'You are unauthorized',
      'page_not_found': 'Page not found',
      'user': {
        'ban': 'User "{name}" has been banned',
        'unregistered': 'Sorry, {name}. You are not registered. ',
      }
    }
  },
  'form': {
    'fields': {
      'labels': {
        'name': 'Name',
        'password': 'Password',
      },
      'placeholders': {
        'name': 'Enter name',
      },
      'errors': {
        'required': 'Field is required',
        'text_min': 'Minimum characters - {min}',
      },
    },
  },
  'multiform': {
    'errors': {
      'empty': 'To submit a multiform, you must create at least one form.',
    },
  }
}
