import {requestStatues} from "../../constants/request-statuses";

export const friends = (app) => {
  app.post('/friends/create', (req, res) => {
    const formsErrors = {forms: []};

    req.body.data.forEach(form => {
      formsErrors.forms.push({
        fieldsErrors: {
          name: {
            'request.errors.user.ban': {name: form.name}
          },
        },
      });
    });

    return res.status(requestStatues.UNPROCESSABLE_ENTITY).send(formsErrors);
  })
}
