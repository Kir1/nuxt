import {requestStatues} from "../../constants/request-statuses";

export const books = (app) => {
  app.get('/books/:genre/:book', (req, res) => {
    const {genre, book} = req.params;
    if (genre === 'drama' && book === 'gamlet') {
      res.json({
        book: {
          title: {
            en: 'Gamlet',
            ru: 'Гамлет',
          },
          text: {
            en: '<b>Hamlet</b> is a tragedy of William Shakespeare in five acts, one of his most famous plays and one ' +
              'of the most famous plays in world drama [1]. Written in 1600-1601. It is the longest play by Shakespeare, with 4,042 lines and 29,551 words.',
            ru: '<b>Га́млет</b> — трагедия Уильяма Шекспира в пяти актах, одна из самых известных его пьес и одна ' +
              'из самых знаменитых пьес в мировой драматургии[1]. Написана в 1600—1601 годах. Это самая длинная пьеса ' +
              'Шекспира — в ней 4042 строки и 29 551 слово.',
          },
        }
      });
    }
    return res.status(requestStatues.NOT_FOUND).send();
  })

  app.post('/books/to-get-mark', (req, res) => {
    const {text} = req.body;
    if (text) {
      let grade = 0;
      if (text === 'var1') grade = 5;
      if (text === 'var2') grade = 3;
      if (grade > 0) res.json({grade});
    }
    return res.status(requestStatues.INTERNAL_SERVER_ERROR).send();
  })
}
