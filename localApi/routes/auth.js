import jsonwebtoken from 'jsonwebtoken';
import {requestStatues} from "../../constants/request-statuses";

export const auth = (app) => {
  const refreshTokens = {};
  let user = null;

  app.post('/auth/login', (req, res) => {
    const {name} = req.body;

    if (name === 'user') {
      return res.status(requestStatues.UNPROCESSABLE_ENTITY).send(
        {
          fieldsErrors: {
            name: {
              'request.errors.user.ban': {name}
            },
          },
        }
      )
    }

    if (name === 'user2') return res.status(requestStatues.INTERNAL_SERVER_ERROR).send();

    if (!(name === 'admin')) {
      return res.status(requestStatues.UNPROCESSABLE_ENTITY).send(
        {
          formErrors: [
            {
              'key': 'request.errors.user.unregistered',
              'options': {name},
            },
          ],
        }
      )
    }

    const expiresIn = 20;
    const refreshToken = Math.floor(Math.random() * (1000000000000000 - 1 + 1)) + 1;

    const accessToken = jsonwebtoken.sign(
      {
        name,
        scope: ['test', 'user']
      },
      'dummy',
      {
        expiresIn
      }
    )

    refreshTokens[refreshToken] = {
      accessToken,
      user: {
        name
      }
    }

    user = {user: {name}};

    res.json({
      token: {
        accessToken,
        refreshToken
      }
    })
  });

  app.get('/auth/user', (req, res) => {
    res.json(user);
  })

  app.post('/auth/logout', (req, res) => {
    res.json({status: 'OK'})
  })
}
