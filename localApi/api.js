import express from 'express';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import {auth} from "./routes/auth";
import {friends} from "./routes/friends";
import {books} from "./routes/books";
const { expressjwt: jwt } = require("express-jwt");

const app = express()

app.use(cookieParser())
app.use(bodyParser.json())

app.use(
  jwt({
    secret: 'dummy',
    algorithms: ['sha1', 'RS256', 'HS256']
  }).unless({
    path: ['/api/auth/login', '/api/auth/user', '/api/auth/logout']
  })
)

auth(app);
friends(app);
books(app);

export default {
  path: '/api',
  handler: app
}
