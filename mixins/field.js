import FieldContainer from "~components/ui/form/fields/FieldContainer.vue";

export const field = {
  props: {
    field: {
      type: Object,
      required: true
    },
  },
  inheritAttrs: false,
  components: {
    FieldContainer
  },
  inject: {
    getFormIndex: {default: null}
  },
  computed: {
    placeholder() {
      return this.field.placeholder ? this.$t(this.field.placeholder) : '';
    },
  },
  methods: {
    onInput(event) {
      // для мультиформы передаётся айди формы
      const formIndex = this.getFormIndex ? this.getFormIndex() : null;
      if (formIndex === null) {
        this.field.onInput(event.target.value);
        if (this.haveValidate()) this.field.validate(event.target.value);
      } else {
        this.field.onInput(event.target.value, formIndex);
        if (this.haveValidate()) this.field.validate(event.target.value, formIndex);
      }
    },
    haveValidate() {
      return 'validate' in this.field;
    },
  },
}


