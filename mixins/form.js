import {fieldStatuses} from "~utils/field/field";

export const form = {
  methods: {
    formCreate(form) {
      return {
        ...form,
        request: false
      };
    },
    async formRequest(form, request, successAction = null, errorAction = null) {
      this.formRemoveErrors(form);

      const isValid = this.formValidate(form);

      if (isValid) {
        form.request = true;

        await request(this.formGetValues(form)).then((data) => {
          if (successAction) successAction(data.response.data);
        }).catch((data) => {
          this.formErrorsEmbedding(data, form, errorAction);
        });

        form.request = false;

      }
    },
    formErrorsEmbedding(data, form, errorAction = null) {
      const fieldsErrors = data?.response?.data?.fieldsErrors;
      const formErrors = data?.response?.data?.formErrors;
      if (fieldsErrors) {
        const formFields = form.fields;
        for (const field in fieldsErrors) {
          formFields[field].errorsBackend = {...formFields[field].errorsBackend, ...fieldsErrors[field]};
        }
        if (errorAction) errorAction(data);
      } else if (formErrors) {
        formErrors.forEach(error => {
          this.$toast.global.error(this.$t(error.key, error.options));
        })
      }
    },
    formValidate(form, index = null) {
      let field;
      let isValid = true;
      for (const key in form.fields) {
        field = form.fields[key];
        if ('validate' in field) {
          if (index !== null) field.validate(field.value, index);
          else field.validate(field.value);
          if (isValid && Object.keys(field.errorsFrontend).length > 0) isValid = false;
        }
      }
      return isValid;
    },
    formRemoveErrors(form) {
      for (const field in form.fields) {
        form.fields[field] = {
          ...form.fields[field],
          errorsFrontend: {},
          errorsBackend: {},
          status: fieldStatuses.FIELD_STATUS_DEFAULT
        };
      }
    },
    formGetValues(form) {
      const cloneForm = {...form};
      const values = {};
      for (const key in cloneForm.fields) {
        values[key] = cloneForm.fields[key].value;
      }
      return values;
    },
  },
}

export const multiform = {
  mixins: [form],
  methods: {
    multiformAddForm(multiform) {
      const newForm = this.$deepCopy(multiform.formTemplate);
      for (const field in newForm.fields) {
        newForm.fields[field].formIndex = multiform.forms.length;
      }
      multiform.forms.push(newForm);
    },
    multiformRemoveForm(multiform, index) {
      multiform.forms.splice(index, 1);
    },
    multiformErrorsEmbedding(data, forms, errorAction) {
      const formsErrors = data?.response?.data?.forms;
      if (formsErrors) {
        formsErrors.forEach((formErrors, index) => {
          const data = {
            response: {
              data: {...formErrors}
            }
          };
          this.formErrorsEmbedding(data, forms[index]);
        });
        if (errorAction) errorAction(data);
      }
    },
    multiformGetValues(forms) {
      const values = [];
      forms.forEach((form, index) => {
        values[index] = this.formGetValues(form);
      });
      return values;
    },
    multiformValidate(multiform) {
      let isValid = true;
      multiform.forms.forEach((form, index) => {
        if (!this.formValidate(form, index) && isValid) isValid = false;
      })
      return isValid;
    },
    multiformRemoveErrors(forms) {
      forms.forEach((form) => {
        this.formRemoveErrors(form);
      })
    },
    async multiformRequest(multiform, request, successAction = null, errorAction = null) {
      const forms = multiform.forms;

      const isValid = this.multiformValidate(multiform);

      if (isValid) {
        const formsValues = this.multiformGetValues(forms);

        if (formsValues.length > 0) {
          this.multiformRemoveErrors(forms);

          multiform.request = true;

          await request(formsValues).then((data) => {
            if (successAction) successAction(data.response.data);
          }).catch((data) => {
            this.multiformErrorsEmbedding(data, forms, errorAction);
          });

          multiform.request = false;
        } else {
          this.$toast.global.error(this.$t('multiform.errors.empty'));
        }
      }
    },
  }
}

export const childForm = {
  props: {
    formIndex: {
      type: Number | String,
      required: true
    },
    name: {
      type: String,
      required: true
    },
    form: {
      type: Object,
      required: true
    },
  },
  methods: {
    idField(name) {
      return `${this.idForm}--${name}`;
    },
  },
  computed: {
    idForm() {
      return `${this.name}-${this.formIndex}`;
    },
  },
  provide() {
    return {
      getFormIndex: () => this.formIndex
    }
  }
}
