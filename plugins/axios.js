import {requestStatues} from "../constants/request-statuses";

export default function ({$axios, app, $toast, redirect, $auth}) {
  $axios.onRequest(settings => {
    // Nuxt Auth и так передаёт Bearer в куках. Вариант ниже просто для примера исп-я в плагине
    if (app.$auth.strategy.token.status().valid()) {
      $axios.setToken(app.$auth.strategy.token.get().accessToken, 'Bearer');
    } else $axios.setToken(false);
  })

  $axios.onError(error => {
    const status = error?.response?.status;

    if (status === undefined) {
      $toast.global.error(app.i18n.t('request.errors.disconnect'));
      return;
    }

    let key = '';
    let requestUrl = '';

    const errorName = error.response?.data?.error?.name;

    if (status === requestStatues.UNAUTHORIZED || errorName === 'UnauthorizedError') {
      key = 'request.errors.unauthorized';
      $auth.logout();
    } else if (status === requestStatues.FORBIDDEN) {
      key = 'request.errors.forbidden';
      requestUrl = '/';
    } else if (status >= requestStatues.INTERNAL_SERVER_ERROR) {
      key = 'request.errors.internal_server_error';
    } else if (status === requestStatues.NOT_FOUND) {
      requestUrl = '/404';
    }

    if (requestUrl) redirect(requestUrl);
    if (key) $toast.global.error(app.i18n.t(key));
  })
}

