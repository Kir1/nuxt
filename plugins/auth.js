export default function ({app, $auth}) {
  // локализация ссылок
  $auth.onRedirect((to, from) => {
    return app.localePath(to);
  })
}
