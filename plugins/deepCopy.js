const deepCopy = (data) => {
  const type = typeof data;
  let obj;
  if (Array.isArray(data)) {
    obj = [];
  } else if (type === 'object') {
    obj = {};
  } else {
    // No more next level
    return data;
  }
  if (Array.isArray(data)) {
    for (let i = 0, len = data.length; i < len; i++) {
      obj.push(deepCopy(data[i]));
    }
  } else if (type === 'object') {
    for (const key in data) {
      obj[key] = deepCopy(data[key]);
    }
  }
  return obj;
}

export default ({app}, inject) => {
  inject('deepCopy', data => deepCopy(data))
}
