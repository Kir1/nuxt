export const getters = {
  'GRADE': 'grade',
  'TEXT': 'text',
}

export const mutations = {
  'INPUT_TEXT': 'inputText',
  'CHANGE_GRADE': 'changeGrade',
}

export const actions = {
  'TO_GET_MARK': 'toGetMark',
}
