export const createError = (errorKey, errorOptions = {}) => {
  return {
    key: errorKey,
    options: errorOptions
  }
}

export const validatorRequired = (value) => {
  return !!value;
}

export const validatorTextMin = (value, min) => {
  return value.length >= min;
}
