import {fieldValidatorCreate} from "~utils/field/field";
import {validatorRequired, validatorTextMin} from "~utils/validators";

export const fieldValidatorRequired = (value) => {
  return fieldValidatorCreate(() => validatorRequired(value), 'form.fields.errors.required');
}

export const fieldValidatorTextMin = (value, min) => {
  return fieldValidatorCreate(() => validatorTextMin(value, min), 'form.fields.errors.text_min', {min});
}
