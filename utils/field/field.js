import {createError} from "~utils/validators";

export const fieldStatuses = {
  'FIELD_STATUS_DEFAULT': 'FIELD_STATUS_DEFAULT',
  'FIELD_STATUS_SUCCESS': 'FIELD_STATUS_SUCCESS',
  'FIELD_STATUS_ERROR': 'FIELD_STATUS_ERROR',
}

export const fieldCreate = (field) => {
  return {
    ...field,
    errorsFrontend: {},
    errorsBackend: {},
    status: fieldStatuses.FIELD_STATUS_DEFAULT,
  };
}

export const fieldMultiformCreate = (field) => {
  return {
    ...fieldCreate(field),
    formIndex: 0,
  };
}

export const fieldValidatorCreate = (validator, errorKey, errorOptions = {}) => {
  return {
    validate: validator,
    error: createError(errorKey, errorOptions)
  }
}

export const fieldValidate = (value, field, validators) => {
  const fieldClone = {...field};
  validators.forEach((validator) => {
    if (validator.validate()) {
      fieldClone.status = fieldStatuses.FIELD_STATUS_SUCCESS;
      if (validator.error.key in fieldClone.errorsFrontend) delete fieldClone.errorsFrontend[validator.error.key];
    } else {
      fieldClone.status = fieldStatuses.FIELD_STATUS_ERROR;
      fieldClone.errorsFrontend[validator.error.key] = validator.error.options;
    }
  })
  return fieldClone;
}

