import {getters} from "~constants-store/text-evaluation";

export default {
  [getters.GRADE](state) {
    return state.grade;
  },
  [getters.TEXT](state) {
    return state.text;
  },
}
