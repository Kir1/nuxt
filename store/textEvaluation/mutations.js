import {mutations} from "~constants-store/text-evaluation";

export default {
  [mutations.INPUT_TEXT](state, text) {
    state.text = text;
  },
  [mutations.CHANGE_GRADE](state, grade) {
    state.grade = grade;
  },
}

