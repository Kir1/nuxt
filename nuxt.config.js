import {resolve} from 'path';
import ru from './locales/ru';
import en from './locales/en';

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'project',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {hid: 'description', name: 'description', content: ''},
      {name: 'format-detection', content: 'telephone=no'}
    ],
    link: [
      {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'}
    ]
  },

  alias: {
    '~components': resolve(__dirname, './components'),
    '~mixins': resolve(__dirname, './mixins'),
    '~constants': resolve(__dirname, './constants'),
    '~constants-store': resolve(__dirname, './constants/store'),
    '~utils': resolve(__dirname, './utils'),
    '~svg': resolve(__dirname, './assets/svg'),
    '~css': resolve(__dirname, './assets/css'),
    '~fonts': resolve(__dirname, './assets/fonts'),
  },

  env: {
    baseUrl: process.env.BASE_URL || ''
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '~/assets/css/main.scss',
    '~/assets/css/form.scss',
    '~/assets/css/icon.scss',
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: ['~/plugins/deepCopy.js'],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/auth-next',
    '@nuxtjs/i18n',
    '@nuxtjs/dotenv',
    '@nuxtjs/style-resources',
    "@nuxtjs/svg",
    '@nuxtjs/toast',
  ],

  axios: {
    retry: {retries: 0},
  },

  i18n: {
    locales: ['en', 'ru'],
    defaultLocale: 'ru',
    vueI18n: {
      fallbackLocale: 'ru',
      messages: {en, ru}
    }
  },

  toast: {
    position: 'bottom-right',
    register: [
      {
        name: 'error',
        message: message => message,
        options: {
          type: 'error',
          theme: 'bubble',
          duration: 7000
        }
      }
    ]
  },

  styleResources: {
    scss: [
      './assets/css/variables.scss',
      './assets/css/mixins.scss',
    ]
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},

  serverMiddleware: ['~/localApi/api'],

  proxy: {
    '/api': 'http://localhost:3000',
  },

  auth: {
    redirect: {
      logout: '/login',
    },
    strategies: {
      local: {
        token: {
          property: 'token',
          global: true,
        },
        user: {
          property: 'user',
        },
        endpoints: {
          login: {url: '/api/auth/login', method: 'post'},
          logout: {url: '/api/auth/logout', method: 'post'},
          user: {url: '/api/auth/user', method: 'get'}
        },
      },
    },
    plugins: [{src: '~/plugins/axios', ssr: true}, '~/plugins/auth.js']
  }
}
